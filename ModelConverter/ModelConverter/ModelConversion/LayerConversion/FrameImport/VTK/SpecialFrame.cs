﻿using Kitware.VTK;

namespace ModelConversion.LayerConversion.FrameImport.VTK
{
    class SpecialFrame : VTKFrame
    {
        public SpecialFrame(string inputPath) : base(inputPath)
        {
            ImportVertices();
            ImportIndices();
        }
    }
}

﻿using System.Linq;
using Kitware.VTK;
using System.IO;
using ModelConversion.LayerConversion.FrameImport.VTK;

namespace ModelConversion.LayerConversion.FrameImport.VTK
{
    class AnatomyRGBFrame : VTKFrame
    {
        private int numberOfPoints;
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AnatomyRGBFrame(string inputPath, float[] rgb) : base(inputPath)
        {
            ImportVertices();
            ImportIndices();
            numberOfPoints = vtkModel.GetNumberOfPoints();

            if (rgb.Length < 3)
                throw Log.ThrowError("Wrong length: " + rgb.Length.ToString(), new IOException());

            CreateColorSurface(rgb);
        }

        private void CreateColorSurface(float[] rgb)
        {
            // Kitware.VTK.dll automatically scales colours to 0-255 range.
            //Scalars = new double[numberOfVertices][];
            Scalars = new double[numberOfPoints][];
            for (int i = 0; i < numberOfPoints; i++)
            {
                double[] currentScalars = { rgb[0], rgb[1], rgb[2] };
                Scalars[i] = currentScalars;
            }
        }
    }
}

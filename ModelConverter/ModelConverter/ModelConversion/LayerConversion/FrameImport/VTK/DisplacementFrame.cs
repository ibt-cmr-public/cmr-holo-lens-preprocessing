﻿using System.Linq;
using Kitware.VTK;

namespace ModelConversion.LayerConversion.FrameImport.VTK
{
    class DisplacementFrame : VTKFrame
    {
        private int numberOfVertices;
        private int numberOfPoints;

        public DisplacementFrame(string inputPath) : base(inputPath)
        {
            ImportVertices();
            ImportIndices();
            numberOfVertices = vtkModel.GetNumberOfCells();
            numberOfPoints = vtkModel.GetNumberOfPoints();
            ImportDisplacementColors();
        }

        private void ImportDisplacementColors()
        {
            // Kitware.VTK.dll automatically scales colours to 0-255 range.
            //Scalars = new double[numberOfVertices][];
            Scalars = new double[numberOfPoints][];
            vtkPointData pointData = vtkModel.GetPointData();
            int arrayNumbers = pointData.GetNumberOfArrays();
            vtkDataArray alphaAngles = pointData.GetScalars("Displacement");
            for (int i = 0; i < numberOfPoints; i++)
            {
                double[] currentScalars = { alphaAngles.GetTuple1(i), 0.0, 0.0 };
                Scalars[i] = currentScalars;
            }
        }

        private double[] TranslateColorAsFraction(double[] scalars)
        {
            for (int i = 0; i < 3; i++)
            {
                scalars[i] = scalars[i] / 255;
            }
            return scalars;
        }
    }
}

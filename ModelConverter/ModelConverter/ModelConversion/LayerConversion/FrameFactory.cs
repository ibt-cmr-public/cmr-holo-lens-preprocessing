﻿using System.IO;

using ModelConversion.LayerConversion.FrameImport.VTK;

namespace ModelConversion.LayerConversion.FrameImport
{
    class FrameFactory
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string extension;

        public IFrame Import(string inputPath, string dataType, float[] rgb= null)
        {
            extension = Path.GetExtension(inputPath);
            switch (extension)
            {
                case ".vtk":
                case ".vtu":
                case ".vtp":
                    return ImportVTKFrame(inputPath, dataType, rgb);
                default:
                    throw Log.ThrowError("Wrong file extension! Only supporting VTK files", new IOException());       
            }
        }

        private IFrame ImportVTKFrame(string inputPath, string dataType, float[] rgb)
        {
            switch (dataType)
            {
                case "anatomy":
                    return new AnatomyFrame(inputPath);
                case "anatomyRGB":
                    return new AnatomyRGBFrame(inputPath, rgb);
                case "fibre":
                    return new FibreFrame(inputPath);
                case "flow":
                    return new FlowFrame(inputPath);
                case "turbulence":
                    return new TurbulenceFrame(inputPath);
                case "displacement":
                    return new DisplacementFrame(inputPath);
                case "special":
                    return new SpecialFrame(inputPath);
                default:
                    throw Log.ThrowError("Wrong model datatype in ModelInfo.json! \n Currently supporting: \"anatomy\" \"fibre\" \"flow\" and \"turbulence\" ", new IOException());
            }
        }
    }
}
